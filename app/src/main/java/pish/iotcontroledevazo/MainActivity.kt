package pish.iotcontroledevazo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*
import pish.iotcontroledevazo.MyFirebaseMessagingService.Companion.FLOW_RATE_EXTRAS
import pish.iotcontroledevazo.MyFirebaseMessagingService.Companion.ON_FLOW_RATE_CHANGED

class MainActivity : AppCompatActivity() {

    private val onNotificationReceivedBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            tvSome.text = intent?.getDoubleExtra(FLOW_RATE_EXTRAS, 0.0).toString()
        }
    }
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                onNotificationReceivedBroadcastReceiver,
                IntentFilter(ON_FLOW_RATE_CHANGED)
            )

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { it ->
            it?.token?.let {
                Log.i("token", it)
                saveTokenOnCloudDatabase(it)
            }
        }
    }

    private fun saveTokenOnCloudDatabase(token: String) {
        db.collection("Dispositivos").document("0Wes0qhmHoDybRM5Mt97")
            .update(mapOf("fcmToken" to token))
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(onNotificationReceivedBroadcastReceiver)
        super.onDestroy()
    }
}
