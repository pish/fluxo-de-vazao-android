package pish.iotcontroledevazo

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val db = FirebaseFirestore.getInstance()

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        message.data["vazao_de_agua"]?.toDoubleOrNull()?.let { flowRate ->
            if (!sendFlowRateBroadcast(flowRate)) {
                sendNotification(flowRate.toString())
            }
        }
    }

    private fun sendFlowRateBroadcast(flowRate: Double): Boolean {
        return LocalBroadcastManager.getInstance(this)
            .sendBroadcast(Intent(ON_FLOW_RATE_CHANGED).apply {
                putExtra(FLOW_RATE_EXTRAS, flowRate)
            })
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        saveTokenOnCloudDatabase(token)
    }

    private fun saveTokenOnCloudDatabase(token: String) {
        db.collection("Dispositivos").document("0Wes0qhmHoDybRM5Mt97")
            .update(mapOf("fcmToken" to token))
    }

    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val channelId = "fluxoDeVazao"
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Fluxo de vazão")
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setPriority(-2)
            .setSound(null)
            .setVibrate(longArrayOf())
            .setContentIntent(pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Fluxo de vazao",
                NotificationManager.IMPORTANCE_LOW
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0, notificationBuilder.build())
    }

    companion object {
        const val ON_FLOW_RATE_CHANGED = "ON_FLOW_RATE_CHANGED"
        const val FLOW_RATE_EXTRAS = "FLOW_RATE_EXTRAS"
    }
}
